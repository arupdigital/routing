import pandas as pd
import numpy as np
import random

class Graph:
    """
    Graph class holds map data as nodes and edges (points and lines), there being a pt at each path corner

    """
    def __init__(self):
        self.graph = dict()

    def add_edge(self, node1, node2, weight=1):
        """
    :param string node1: key from dict, node label
    :param float weight: edge weight...
    :returns: none

    Adds a directed edge from `node1` to `node2` to the graph with weight defined by `weight`.
    """
        if node1 in self.graph.keys():
          q = False
          for i in self.graph[node1]:
            if i == node2:
              # j = weight
              q = True
              break

          if not q:
            self.graph[node1].append(node2)
        else:
          self.graph[node1] = [node2]


    def has_edge(self, node1, node2):
        """
    :param string node1, node2: key node labels from dict
    :returns: bool

    Returns whether the graph contains an edge from `node1` to `node2`.
    """

        if node1 not in self.graph:
            return False
        return node2 in [x for x in self.graph[node1]]

    def get_neighbors(self, node) -> list:
        """
    :param string node: key label of dict
    :returns: list

    Returns the neighbors of `node` as a list of tuples [(x, y), ...] where
    `x` is the neighbor node, and `y` is the weight of the edge from `node`
    to `x`.
    """
        try:
            r = self.graph[node][:]
        except Exception:
            r = list()
        return r


def prepare(file: str) -> pd.DataFrame:
    """
    Import and cleans data, gets it into right format, Pandas Dataframe.
    :param file: the path to file. Expects a path to csv file.
    :return: Data frame from file.
    """
    # assert file is csv
    # if both are null, remove
    # output pandas from csv
    data = pd.read_csv(file)
    data = data.set_index(data["id"], drop=True)
    # data.set_index()
    data = data[data['floor'] == 4]
    # data.
    # data.drop("id", axis=1)
    try:
        data = data.dropna(subset=['zone', 'prevzone']) # drop rows where both are null
    except KeyError:
        print(KeyError, 'when ')
        data = data.dropna(subset=['zone', 'prevzone'])
            # print(e.)
    finally:
        data.replace("Galery","Gallery", inplace=True, regex=True) # fix spelling error in data
    return data

# descending id's is advancing in time.....
def check_adjacency(node: str, neighbor: str) -> bool:
    """
    Check to see if neighbor is actually a aneighbor of node.
    :param node:
    :param neighbor:
    :return:
    """
    return neighbor in graph.get_neighbors(node)

def compute_segment_counts(data: pd.DataFrame) -> pd.DataFrame:
    """
    Higher level function for taking in data, finding routes, and segment counts. see comments
    :param data:
    :return:
    """
    # adds boolean column (adjacent or not?)
    data["adjacency"] = data.apply(lambda x: check_adjacency(x["zone"], x["prevzone"]), axis=1)

    ### the long chain (see worksheet spreadsheet) ###

    # create copy of !adjacent (the long chains)
    _data = data[data["adjacency"]== False].copy()

    # adds column filled with path lists
    _data['route'] = _data.apply(lambda x: pathfinder(x['prevzone'],x['zone'])['path'], axis =1)
    _data.to_csv("output/long_chains.csv") # output

    # add another column of DataFrame objects (embedded inside the Dataframe)
    _data['route_df'] = _data.apply(lambda x: path_to_segments_df(x['route']), axis = 1) # Takes each route (a list)(1 per row), and turns it into a dataframe, embedded inside each row in _data datafram...so each cell has another dataframe inside it...
    new_segments = pd.concat(_data['route_df'].to_list()) # concatenate column of dataframes into one dataframe...converts column of dataframes into list of dataframes, then concatenates it.
    new_segments = new_segments[['prevzone','zone', 'adjacency']] # get slice of new table
    data = data[data["adjacency"]== True]
    data = pd.concat([new_segments, data]) # combinei old and new tables into one large new table of adjacent-node segments

    # Aggregate segemnts into counts, opposite segments are counted as same (2x).
    # data[['zone', 'prevzone']] = np.sort(data[['zone', 'prevzone']], axis=1) # this aligns same nodes in prev/zone to count both directionsp when groupedpp
    data = data.groupby(['zone', 'prevzone']).size() # creates a multiindex series with count
    return data

class Tracker(object):
    """
    Tracker class is the graph traversing object, like a spider on a web. This object uses a modified depth first search. 
    Each node in a path is randomly selected, to produce a distribution of possible paths between two galleries. Tracker first discovers a node, then traverses it (or crosses it), then discovers the next one, etc.  
    Remember, the basic sequence is 
        1. get nodes neighbors
        2. add them to 'stack' (last in first out) (added as node/neighbor edges)
        3. 'discover' next thing on stack ; parent added to discovered table. Discovered table continues to be filled until target is found.
        4. if determined to continue, traverse discovered edge
        5. Continue until the edge unstacked from stack is to the target.

    """

    def __init__(self, source, target):
        assert type(source) == str
        self.source = (source, source)
        self.target = target
        self.the_stack = [self.source]
        self.discovered = dict()
        self.count = len(self.discovered.keys())


    def stack(self, node: str, parent: str) -> None:
        """ Stack items are stored as node/parent tuples"""
        assert type(node) == str, "type error!"
        assert type(parent) == str, "type error!"
        self.the_stack.append((node, parent))

    def unstack(self) -> tuple:
        node_pair = self.the_stack.pop(-1)
        return node_pair

    def discover(self, edge: tuple) -> str:
        '''
        Node discovery: based on node, checks what next step is. returns next step as message. 
        :param edge:
        :return:
        '''
        assert type(edge) == tuple, f"typerror {edge}"
        parent = edge[1]
        node = edge[0]
        if node not in self.discovered.keys():
            self.discovered[node] = parent
            if node == self.target:
                return 'success'
            self.traverse(node)
            return 'continue'
        if node == self.target:
            return 'success'
        elif node in self.discovered.keys():
            return 'continue'
        else:
            print("ALERT")

    def traverse(self, node: str) -> None:
        '''
        Add node's neighbors to stack in a random order
        :param node:
        :return:
        '''
        assert type(node) == str, node
        neighbors = graph.get_neighbors(node)
        while neighbors:
            n = neighbors.pop(
                neighbors.index(random.choice(neighbors))
            )
            self.stack(n, node)

    def walk_to(self) -> None:
        """
        Find random walk from instance source to target. from the discovered table.
        produces path to node and total distance from the discovered table
        """
        # assert type(node) == str, "type error"
        node = self.target
        ls = [node]
        if (node in self.discovered.keys()) and self.discovered[node] == node:
            return ls

        def construct_path(node):
            if (node in self.discovered.keys()) and self.discovered[node] == node:
                ls.reverse()
                return
            else:
                ls.append(self.discovered[node])
                construct_path(self.discovered[node])

        construct_path(node)
        return ls

def pathfinder(source : str, target : str) -> dict:
    '''
    top level function for graph search. finds a valid random path from source to target. This is where the actual DFS algorithm is.
    :param source: Node Label
    :param target: Node Label
    :return:
    '''
    assert type(source) == str, source
    assert type(target) == str, target
    t = Tracker(source, target)
    while t.the_stack:
        node = t.unstack()
        # print(node)
        status  = t.discover(node)
        if status == 'success':
            return {
                "tracker": t,
                "path": t.walk_to()
            }
        if status == 'continue':
            continue
    return {
            "tracker": t,
            "path": list()
    }

def path_to_segments_df(path : list) -> pd.DataFrame:
    '''
    Creates a Dataframe of segments (as rows) from a sequence of nodes. THis is to separate out the path node list into rows for the data table. 
    :param path: List of nodes
    :return: Dataframe
    '''
    # path = [1, 2, 3, 4, 5]
    frame = list()
    for i, j in enumerate(path[:-1]):
        assert type(j) == str, path
        x = dict()
        x['prevzone'] = path[i]
        x['zone'] = path[i + 1]
        x['adjacency'] = True
        frame.append(x)
    return pd.DataFrame(frame)

def build_graph() -> None:
    """
    Builds a Graph from graph csv file. Enforces Graph integrity (that neighbors of node contain node as neighbor) see csv file for guidance on how to make csv. 
    """
    fourth_floor = pd.read_csv("data/graph.csv")
    fourth_floor = fourth_floor.fillna('')
    fourth_floor_count = fourth_floor.count()[0] # find length for iterating

    for k in range(2):
        for i in range(fourth_floor_count):
            row = fourth_floor.iloc[i]
            node = row[0]
            for n in row[1:]: #series neighbors
                if pd.isna(n) or n == '':
                    continue
                if k == 0: # adding edges to graph
                    graph.add_edge(node,n)
                if k == 1: # checking for reverse edges
                    assert graph.has_edge(n, node), print(f'looking for {node} in neighbors of {n}:{graph.graph[n]}')


if __name__ == "__main__":
    graph = Graph()
    build_graph()
    data = prepare("data/zone_visits.csv")
    compute_segment_counts(data).to_csv("output/segment_counts.csv")

